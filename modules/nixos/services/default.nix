{ ... }:
{
  imports = [
    ./mumble.nix
    ./taler.nix
  ];
}
